import express from 'express';
import { json } from 'body-parser';
import helmet from 'helmet';
import graphqlHTTP from 'express-graphql';
import { DbContext } from './data/db-context';
import { authenticationMiddleware } from './utils/middlewares/authenticationMiddleware';

import schema from './graphql/schemas/schema';
import resolver from './graphql/resolvers/resolver';

class App {
    public express: express.Application;
    private context: DbContext;

    constructor() {
        this.startup();
        this.mountRoutes();
    }

    private mountRoutes(): void {
        const router = express.Router();

        router.get('/', (req: any, res: any) => {
            res.json({
                message: 'It does work.'
            });
        });
        this.express.use('/', router);
    }

    private establishConnection(): void {
        this.context = new DbContext();
        this.context.connect();
    }

    private startup(): void {
        this.express = express();
        this.express.use(json());
        this.express.use(helmet());
        this.express.use(authenticationMiddleware);
        this.setUpGraphql();
        this.establishConnection();
    }

    private setUpGraphql(): void {
        this.express.use('/graphql', graphqlHTTP({
            schema: schema,
            rootValue: resolver,
            graphiql: true
        },
        ));
    }
}

export default new App().express;
