import { User } from './User';

export class Happening {

    private _title: String;
    private _description: String;
    private _price: Number;
    private _date: String;
    private _creator: User;

    public get title(): String {
        return this._title;
    }

    public get description(): String {
        return this._description;
    }

    public get price(): Number {
        return this._price;
    }

    public get date(): String {
        return this._date;
    }

    public get creator(): User {
        return this._creator;
    }

    constructor(title: String, description: String,
        price: Number, date: String, creator: User) {
        this._title = title;
        this._description = description;
        this._price = price;
        this._date = date;
        this._creator = creator;
        }
    }
