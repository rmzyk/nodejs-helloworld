import { Happening } from './Happening';

export class User {

    private _email: String;
    private _password: String;
    private _createdHappenings: Array<Happening>;

    public get email(): String {
        return this._email;
    }

    public get password(): String {
        return this._password;
    }

    public get createdHappenings(): Array<Happening> {
        return this._createdHappenings;
    }

    constructor(email: String, password: String,
        createdHappenings: Array<Happening>) {
        this._email = email;
        this._password = password;
        this._createdHappenings = createdHappenings;
        }
    }
