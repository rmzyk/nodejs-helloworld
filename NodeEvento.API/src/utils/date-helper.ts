export class DateHelper {

    public static toISOString(parameter: string | Date): string {
        return new Date(parameter).toISOString();
    }
}
