import bcrypt from 'bcrypt';

export default class PasswordHasher {

    public static hashPassword(password: string, salt: number,
         callback: (error: Error, hash: string) => void): void {
            bcrypt.hash(password, salt, (error, hash) => {
            callback(error, hash);
        });
    }

    public static comparePasswrods(password: string, dbHash: string, callback: (error: string | null, match: boolean | null) => void) {
        bcrypt.compare(password, dbHash, (err: Error, match: boolean) => {
            if (match) {
                callback(null, true);
            } else {
                callback('Invalid password match', null);
            }
        });
    }
}
