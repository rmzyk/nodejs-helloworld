import { buildSchema } from 'graphql';

export default buildSchema(`
        type Happening {
            _id: ID!
            title: String!
            description: String!
            price: Float!
            date: String!
            creator: User!
        }

        input HappeningArgs {
            title: String!
            description: String!
            price: Float!
            date: String!
        }

        type RootQuery {
            happenings: [Happening!]!
        }

        type RootMutation {
            createHappening(happeningArgs: HappeningArgs): Happening
        }

        schema {
            query: RootQuery
            mutation: RootMutation
        }`);
