import { buildSchema } from 'graphql';

export default buildSchema(`
        type Booking {
            _id: ID!
            happening: Happening!
            user: User!
            createdAt: String!
            updatedAt: String!
        }

        type RootQuery {
            bookings:   [Booking!]!
        }

        type RootMutation {
            bookHappening(happeningId: ID!): Booking!
            cancelBooking(bookingId: ID!): Happening!
        }

        schema {
            query: RootQuery
            mutation: RootMutation
        }`);
