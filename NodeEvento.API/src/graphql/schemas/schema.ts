import { buildSchema } from 'graphql';

export default buildSchema(`
        type Happening {
            _id: ID!
            title: String!
            description: String!
            price: Float!
            date: String!
            creator: User!
        }

        type User {
            _id: ID!
            email: String!
            password: String
            createdHappenings: [Happening!]
        }

        type Booking {
            _id: ID!
            happening: Happening!
            user: User!
            createdAt: String!
            updatedAt: String!
        }

        type AuthData {
            userId: ID!
            token: String!
            tokenExpiration: Int!
        }

        input HappeningArgs {
            title: String!
            description: String!
            price: Float!
            date: String!
        }

        input UserArgs {
            email: String!
            password: String!
        }

        type RootQuery {
            happenings: [Happening!]!
            bookings:   [Booking!]!
            login(email: String!, password: String!): AuthData!
        }

        type RootMutation {
            createHappening(happeningArgs: HappeningArgs): Happening
            createUser(userArgs: UserArgs): User
            bookHappening(happeningId: ID!): Booking!
            cancelBooking(bookingId: ID!): Happening!
        }

        schema {
            query: RootQuery
            mutation: RootMutation
        }`);
