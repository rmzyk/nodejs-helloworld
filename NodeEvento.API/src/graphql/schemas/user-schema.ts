import { buildSchema } from 'graphql';

export default buildSchema(`
        type User {
            _id: ID!
            email: String!
            password: String
            createdHappenings: [Happening!]
        }

        input UserArgs {
            email: String!
            password: String!
        }

        type RootMutation {
            createUser(userArgs: UserArgs): User
        }

        schema {
            query: RootQuery
            mutation: RootMutation
        }`);
