import { bookingResolver } from './resolvers/booking-resolver';
import { userResolver } from './resolvers/user-resolver';
import { happeningResolver} from './resolvers/happening-resolver';

import bookingSchema from './schemas/booking-schema';
import userSchema from './schemas/user-schema';
import happeningSchema from './schemas/happening-schema';

export const rootResolver = {
        ...bookingResolver,
        ...userResolver,
        ...happeningResolver
};

export const rootSchema = `${bookingSchema}${userSchema}${happeningSchema}`;

