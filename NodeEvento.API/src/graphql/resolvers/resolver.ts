import { UserRepository } from '../../data/repositories/user-repository';
import HappeningModel from '../../data/schemas/happening-schema';
import UserModel from '../../data/schemas/user-schema';
import BookingModel from '../../data/schemas/booking-schema';
import bcrypt from 'bcrypt';
import { IUser } from '../../data/contracts/user-contract';
import { IBooking } from '../../data/contracts/booking-contract';
import { HappeningRepository } from '../../data/repositories/happening-repository';
import jsonwebtoken, { TokenExpiredError } from 'jsonwebtoken';

export default {
    happenings: async () => {
        try {
            const happenings = await HappeningModel.find();
            return happenings.map(happening => {
                return {
                    _id: happening._id,
                    creator: UserRepository.getUser(happening.creator._id),
                    description: happening.description,
                    title: happening.title,
                    price: happening.price,
                    date: new Date(happening.date).toISOString(),
                };
            });
        } catch (error) {
            throw error;
        }
    },
    bookings: async (args: any, req: any) => {
        if (!req.isAuth) {
            throw new Error('Unauthenticated');
        }
        try {
            const bookings = await BookingModel.find();
            return bookings.map(book => {
                return <IBooking>{
                    _id: book._id,
                    user: UserRepository.getUser(book.user._id),
                    happening: HappeningRepository.getHappening(book.happening._id),
                    createdAt: new Date(book.createdAt).toISOString(),
                    updatedAt: new Date(book.updatedAt).toISOString(),
                    ...book
                };
            });
        } catch (error) {
            throw error;
        }
    },
    createHappening: async (args: any, req: any) => {
        if (!req.isAuth) {
            throw new Error('Unauthenticated');
        }
        try {
            const happening = new HappeningModel({
                title: args.happeningArgs.title,
                description: args.happeningArgs.description,
                price: +args.happeningArgs.price,
                date: args.happeningArgs.date,
                creator: '5e1286ebc8979f386c6a446a'
            });
            let createdHappening;
            const result = await happening.save();

            createdHappening = {
                ...result, _id: result._id,
                date: new Date(happening.date), creator: UserModel.findById(result.creator), title: happening.title
            };
            const userModel = await UserModel.findById('5e1286ebc8979f386c6a446a');
            if (!userModel) {
                throw new Error('User doesnt exist.');
            }
            userModel.createdHappenings.push(happening);
            await userModel.save();
            return createdHappening;
        } catch (error) {
            throw error;
        }
    },
    createUser: async (args: any) => {
        try {
            const user = await UserModel.findOne({ email: args.userArgs.email });
            if (user) {
                throw new Error('User with this email exists already.');
            }
            const hashedPassword = await bcrypt.hash(args.userArgs.password, 12);
            const userModel = new UserModel({
                email: args.userArgs.email,
                password: hashedPassword
            });
            const savedModel = await userModel.save();
            return <IUser>{
                email: savedModel.email,
                password: null
            };
        } catch (error) {
            throw error;
        }
    },
    bookHappening: async (args: any, req: any) => {
        if (!req.isAuth) {
            throw new Error('Unauthenticated');
        }
        try {
            const fetchedHappening = await HappeningModel.findOne({ _id: args.happeningId });
            const booking = new BookingModel({
                user: '5e1286ebc8979f386c6a446a',
                happening: fetchedHappening
            });
            const result = await booking.save();
            return <IBooking>{
                _id: result._id,
                user: UserRepository.getUser(result.happening.creator._id),
                happening: HappeningRepository.getHappening(result.happening._id),
                createdAt: new Date(result.createdAt).toISOString(),
                updatedAt: new Date(result.updatedAt).toISOString(),
                ...result
            };
        } catch (error) {
            throw error;
        }
    },
    cancelBooking: async (args: any, req: any) => {
        if (!req.isAuth) {
            throw new Error('Unauthenticated');
        }
        try {
            const booking = await BookingModel.findById(args.bookingId).populate('UserModel', 'HappeningModel');
            BookingModel.remove(booking);
            return <IBooking>{
                _id: booking._id,
                user: UserRepository.getUser(booking.happening.creator._id),
                happening: HappeningRepository.getHappening(booking.happening._id),
                createdAt: new Date(booking.createdAt).toISOString(),
                updatedAt: new Date(booking.updatedAt).toISOString(),
                ...booking
            };
        } catch (error) {
            throw error;
        }
    },
    login: async ({ email, password }) => {
        try {
            const user = await UserModel.findOne({ email: email });
            if (!user) {
                throw new Error('User does not exist.');
            }
            const isEqual = await bcrypt.compare(password, user.password);
            if (!isEqual) {
                throw new Error('Credentials are incorrect');
            }
            const token = jsonwebtoken.sign(
                {
                    userId: user._id,
                    email: user.email
                },
                'secretkey',
                { expiresIn: '1h' }
            );
            return { userId: user.id, token: token, tokenExpiration: 1 };
        } catch (error) {
            throw error;
        }
    }
};
