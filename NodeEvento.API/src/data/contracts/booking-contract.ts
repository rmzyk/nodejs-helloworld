import mongoose from 'mongoose';
import { IUser } from './user-contract';
import { IHappening } from './happening-contract';

export interface IBooking extends mongoose.Document {
    _id: any;
    user: IUser;
    happening: IHappening;
    createdAt: string | Date;
    updatedAt: string | Date;
}
