import mongoose from 'mongoose';
import { IHappening } from './happening-contract';

export interface IUser extends mongoose.Document {
    _id: any;
    email: string;
    password: string;
    createdHappenings: Array<IHappening>;
}
