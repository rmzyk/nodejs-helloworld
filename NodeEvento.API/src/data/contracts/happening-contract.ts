import mongoose from 'mongoose';
import { IUser } from './user-contract';

export interface IHappening extends mongoose.Document {
    _id: any;
    title: string;
    description: string;
    price: number;
    date: string | Date;
    creator: IUser;
}
