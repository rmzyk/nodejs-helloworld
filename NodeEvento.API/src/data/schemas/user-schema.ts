import mongoose, { Schema } from 'mongoose';
import { IUser } from '../contracts/user-contract';

const userSchema = new Schema({
        email: {
            type: String,
            required: true
        },
        password: {
            type: String,
            required: true
        },
        createdHappenings: [
            {
                type: Schema.Types.ObjectId,
                ref: 'HappeningModel'
            }
        ]});

export default mongoose.model<IUser>('UserModel', userSchema);
