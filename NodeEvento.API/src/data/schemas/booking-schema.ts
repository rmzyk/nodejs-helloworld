import mongoose, { Schema } from 'mongoose';
import { IBooking } from '../contracts/booking-contract';

const bookingSchema = new Schema({
        happening: {
            type: Schema.Types.ObjectId,
            ref: 'HappeningModel'
        },
        user: {
            type: Schema.Types.ObjectId,
            ref: 'UserModel'
        }
    },
    { timestamps: true});

export default mongoose.model<IBooking>('BookingModel', bookingSchema);
