import mongoose, { Schema } from 'mongoose';
import { IHappening } from '../contracts/happening-contract';

const happeningSchema = new Schema({
        title: {
            type: String,
            required: true
        },
        description: {
            type: String,
            required: true
        },
        price: {
            type: Number,
            required: true
        },
        date: {
            type: Date,
            required: true
        },
        creator: {
            type: Schema.Types.ObjectId,
            ref: 'UserModel'
        }
    });

export default mongoose.model<IHappening>('HappeningModel', happeningSchema);
