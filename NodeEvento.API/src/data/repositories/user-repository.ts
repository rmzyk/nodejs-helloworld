import UserModel from '../schemas/user-schema';
import { IUser } from '../contracts/user-contract';
import { HappeningRepository } from './happening-repository';

export class UserRepository {
    public static async getUser(id: any): Promise<IUser> {
        try {
            const user = await UserModel.findById(id);
            const userIds = user.createdHappenings.map(x => x._id);
                return <IUser>{
                     _id: user._id,
                     createdHappenings: HappeningRepository.getHappenings(userIds),
                     email: user.email,
                     password: user.password,
                     ...user
                };
        } catch (error) {
            throw error;
        }
    }
}
