import HappeningModel from '../schemas/happening-schema';
import { IHappening } from '../contracts/happening-contract';
import { UserRepository } from './user-repository';
import { DateHelper } from '../../utils/date-helper';

export class HappeningRepository {
    public static async getHappenings(ids: Array<string>): Promise<IHappening[]> {
        try {
            const happenings = await HappeningModel.find({ _id: { $in: ids } });
            return happenings.map(happening => {
                return this.mapHappening(happening);
            });
        } catch (error) {
            throw error;
        }
    }

    public static async getHappening(id: string): Promise<IHappening> {
        try {
            const happening = await HappeningModel.findOne({ _id: id });
            return this.mapHappening(happening);
        } catch (error) {
            throw error;
        }
    }

    public static mapHappening = happening => {
        return <IHappening>{
            _id: happening._id,
            creator: UserRepository.getUser(happening.creator._id),
            description: happening.description,
            title: happening.title,
            price: happening.price,
            date: DateHelper.toISOString(happening.date),
            ...happening
        };
    }
}
