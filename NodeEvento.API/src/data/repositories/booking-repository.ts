import { IBooking } from '../contracts/booking-contract';
import { DateHelper } from '../../utils/date-helper';
import { HappeningRepository } from './happening-repository';
import { UserRepository } from './user-repository';

export class BookingRepository {
    public static mapBooking = booking => {
        return <IBooking>{
            _id: booking._id,
            user: UserRepository.getUser(booking.happening.creator._id),
            happening: HappeningRepository.getHappening(booking.happening._id),
            createdAt: DateHelper.toISOString(booking.createdAt),
            updatedAt: DateHelper.toISOString(booking.createdAt),
            ...booking
        };
    }
}

