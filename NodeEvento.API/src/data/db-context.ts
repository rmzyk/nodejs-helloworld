import Mongoose from 'mongoose';
import AppSettings from '../app-settings';

export class DbContext {
    constructor() {
        this.setup();
    }

    public dispose(): void {
        Mongoose.disconnect()
            .catch((error) => {
                console.log(error);
            });
    }

    public connect(): void {
        Mongoose.connect(`mongodb://${AppSettings.DB_LOGIN}:${AppSettings.DB_PASSWORD}@localhost:27017/`)
            .then(() => {
                console.log('db connect success');
            })
            .catch((error) => {
                console.log(error);
            });
    }

    private setup(): void {
        Mongoose.set('useNewUrlParser', true);
        Mongoose.set('useUnifiedTopology', true);
    }
}
