import dotenv from 'dotenv';

class AppSettings {

    public readonly DB_PASSWORD: string;
    public readonly DB_LOGIN: string;
    public readonly DB_NAME: string;

    constructor() {
        dotenv.config();

        this.DB_LOGIN = process.env.MONGO_USER;
        this.DB_PASSWORD = process.env.MONGO_PASSWORD;
        this.DB_NAME = process.env.DB_NAME;
    }
}

export default new AppSettings();
