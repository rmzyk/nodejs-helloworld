# Decision: Use Express as framework and node.js as technology.
# Consequences: Have loosly typed language like javascript which can make runtime errors which can make more bugs in production.
# Profits: Really fast development with easy learning curve.