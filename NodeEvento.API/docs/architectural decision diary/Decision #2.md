# Decision: Use Typescript.
# Consequences: We need to transpile typescript to javascript before we use it, so we need more sophisticated development environment.
# Profits: Less bugs due to typescript strong typing.