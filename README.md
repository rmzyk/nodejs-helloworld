# Nodejs-helloworld

Project made with udemy course about node.js technology. The purpose is to learn basics of server side javascript, use mongodb and graphql.
I will also build a client for API made with react.

# To build an application node.js is required

    1. Install node.js
    2. Run command `npm install`
    3. Run command `npm run start`

To deploy an application run command:

    1. `npm run build`


# Troubleshoot

    1.`npm link typescript`
    when error 'Error: Cannot find module 'typescript'' occurs.